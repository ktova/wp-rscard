<?php 
// Add scripts and stylesheets
add_action( 'wp_enqueue_scripts', 'custom_scripts' );
function custom_scripts(){
  wp_enqueue_style( 'font-fredoka-one', 'https://fonts.googleapis.com/css?family=Fredoka+One');
  wp_enqueue_style( 'font-open-sans', 'https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic');

  wp_enqueue_style( 'map-icons', get_template_directory_uri() . '/fonts/map-icons/css/map-icons.min.css' );
  wp_enqueue_style( 'icomoon', get_template_directory_uri() . '/fonts/icomoon/style.css' );
  wp_enqueue_style( 'bxslider', get_template_directory_uri() . '/js/plugins/jquery.bxslider/jquery.bxslider.css' );
  wp_enqueue_style( 'mCustomScrollbar', get_template_directory_uri() . '/js/plugins/jquery.customscroll/jquery.mCustomScrollbar.min.css' );
  wp_enqueue_style( 'mediaelementplayer', get_template_directory_uri() . '/js/plugins/jquery.mediaelement/mediaelementplayer.min.css' );
  wp_enqueue_style( 'fancybox', get_template_directory_uri() . '/js/plugins/jquery.fancybox/jquery.fancybox.css' );
  wp_enqueue_style( 'carousel', get_template_directory_uri() . '/js/plugins/jquery.owlcarousel/owl.carousel.css' );
  wp_enqueue_style( 'owlcarousel', get_template_directory_uri() . '/js/plugins/jquery.owlcarousel/owl.theme.css' );
  wp_enqueue_style( 'optionpanel', get_template_directory_uri() . '/js/plugins/jquery.optionpanel/option-panel.css' );
  wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css' );
  wp_enqueue_style( 'theme-color', get_template_directory_uri() . '/colors/theme-color.css' );

  wp_enqueue_script( 'modernizr', '/js/libs/modernizr.js');
  wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js', [], false, true );
  wp_enqueue_script( 'api', 'https://maps.googleapis.com/maps/api/js', [], false, true );
  wp_enqueue_script( 'site', get_template_directory_uri() . '/js/site.js', [], false, true );
}
add_action( 'after_setup_theme', 'custom_theme_setup' );
function custom_theme_setup(){
  // WordPress Titles
  add_theme_support( 'title-tag' );
  // Article image
  add_theme_support( 'post-thumbnails' );
}
function wpb_custom_new_menu() {
  register_nav_menu('my-custom-menu',__( 'My Custom Menu' ));
}
add_action( 'init', 'wpb_custom_new_menu' );
?>