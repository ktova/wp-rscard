			<!-- START: PAGE CONTENT -->			
			<div class="row animate-up">
				<div class="col-sm-8">
					<main class="post-single">
						<article class="post-content section-box">							
							<div class="post-inner">
								<header class="post-header">
									<div class="post-data">
										<div class="post-tag">
											<a href="single.html">#Photo</a>
											<a href="single.html">#Architect</a>
										</div>

										<div class="post-title-wrap">
											<h1 class="post-title"><?php the_title() ?></h1>
											<time class="post-datetime" datetime="2021-05-01T12:00:00+00:00">
												<span class="day"><?php echo get_the_date('d'); ?></span>
												<span class="month"><?php echo get_the_date('M'); ?></span>
											</time>
										</div>

										<div class="post-info">
											<a href="single.html"><i class="rsicon rsicon-user"></i>by Tova</a>
											<a href="single.html"><i class="rsicon rsicon-comments"></i>56</a>
										</div>
									</div>
								</header>

								<div class="post-editor clearfix">
                                <?php the_content() ?>
							</div><!-- .post-inner -->
						</article><!-- .post-content -->
					</main>
					<!-- .post-single -->
				</div>

				<div class="col-sm-4">
					<?php get_sidebar(); ?>
				</div><!-- .col-sm-4 -->
			</div><!-- .row -->			
			<!-- END: PAGE CONTENT -->