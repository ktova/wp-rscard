<div class="sidebar sidebar-default">
    <div class="widget-area">
        <aside class="widget widget-profile">
            <div class="profile-photo">
                <img src="<?php echo get_field('pic')['url']; ?>" alt="Robert Smith"/>
            </div>
            <div class="profile-info">
                <h2 class="profile-title"><?php echo get_field('name'); ?></h2>
                <h3 class="profile-position"><?php echo get_field('job'); ?></h3>
            </div>
        </aside><!-- .widget-profile -->

                <div class="input-field">
                    <input class="contact-email" type="email" name="email"/>
                    <span class="line"></span>
                    <label>Email</label>
                </div>

                <div class="input-field">
                    <input class="contact-subject" type="text" name="subject"/>
                    <span class="line"></span>
                    <label>Subject</label>
                </div>

                <div class="input-field">
                    <textarea class="contact-message" rows="4" name="message"></textarea>
                    <span class="line"></span>
                    <label>Message</label>
                </div>

                <span class="btn-outer btn-primary-outer ripple">
                    <input class="contact-submit btn btn-lg btn-primary" type="submit" value="Send"/>
                </span>
                
                <div class="contact-response"></div>
            </form>
        </aside><!-- .widget_contact -->

        <?php
        // the query
        $the_query = new WP_Query( array(
        'posts_per_page' => 3,
        ));
        ?>

        <aside class="widget widget-recent-posts">
            <h2 class="widget-title">Recent posts</h2>
            <ul>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
            <li>
              <div class="post-tag">
                <a href="single.html">#Photo</a>
                <a href="single.html">#Architect</a>
              </div>
              <h3 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
              <div class="post-info"><a href="single.html"><i class="rsicon rsicon-comments"></i><?php echo get_comments_number(); ?> comments</a></div>
            </li>
          <?php endwhile; ?>
          <?php wp_reset_postdata(); ?>
            </ul>
        </aside><!-- .widget-recent-posts -->
    </div><!-- .widget-area -->
</div><!-- .sidebar -->