<div class="grid-item">
    <article class="post-box animate-up">

    <?php if ( has_post_thumbnail() ) { ?>
           <div class="post-media">
             <div class="post-image">
               <a href="<?php the_permalink() ?>"><?php the_post_thumbnail() ?> </a>
             </div>
           </div>
       <?php  }else { ?>
           <div class="post-media no-media"></div>
       <?php } ?>

        <div class="post-data">
            <time class="post-datetime" datetime="2015-03-13T07:44:01+00:00">
                <span class="day"><?php echo get_the_date('d'); ?></span>
                <span class="month"><?php echo get_the_date('M'); ?></span>
            </time>

            <div class="post-tag">
                <a href="category.html">#Photo</a>
                <a href="category.html">#Architect</a>
            </div>

            <h3 class="post-title">
                <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
            </h3>

            <div class="post-info">
                <a href="category.html"><i class="rsicon rsicon-user"></i><?php the_author() ?></a>
                <a href="category.html"><i class="rsicon rsicon-comments"></i><?php comments_number() ?></a>
            </div>
        </div>
    </article>
</div>
