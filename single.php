<?php get_header(); ?>

    <!-- START: PAGE CONTENT -->
                <div class="blog">
                    <div class="blog-grid">
                        <div class="grid-sizer"></div>

                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                        <?php get_template_part( 'content-single' ); ?>

                        <?php endwhile; endif; ?>
                    
                    </div><!-- .blog-grid -->
                </div><!-- .blog -->	
				<!-- END: PAGE CONTENT -->
                
                <?php get_footer(); ?>